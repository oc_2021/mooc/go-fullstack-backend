
// ========= Import packages ============ //
const express    = require('express');
const app        = express();
const bodyParser = require('body-parser');
const mongoose   = require('mongoose');

const Thing      = require('./models/Thing');

// ============================================================================================================ //


/**
 * Connection to the MongoDB database
 */
mongoose.connect(
    'mongodb+srv://djo_4_dev:55468755@ocgotonodejs01.307v6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    { useNewUrlParser : true, useUnifiedTopology : true })
    .then(  ()  => console.log('Connexion à MongoDB réussie ! ') )
    .catch( ()  => console.log('Connexion à MongoDB échouée ! ') );

// ============================================================================================================ //


/**
 *  Correction for resolve the CORS error
 */
app.use( (req, res, next) => {

    // Add headers to allow:
    // ------------------------------------ //
    // access our API from any source ('*');
    res.setHeader( 'Access-Control-Allow-Origin', '*' );

    // add headers to requests sent to our API (Origin, X-Requested-With, etc.);
    res.setHeader( 'Access-Control-Allow-Headers', 'Origin, X-Requested-With, ' +
        'Content, Accept, Content-Type, Authorization' );

    // Send requests With the mentioned methods (GET, POST, etc.).
    res.setHeader( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS' );

    // Go to next Middleware
    next();
});


/**
 * Define a global function json as middleware
 */
app.use( bodyParser.json() );



// ============================================================================================================ //
//                                      Middleware for the CRUD API
// ============================================================================================================ //

/**
 * POST Request: Add article in the database MongoDB with Mongoose packega
 */
app.post('/api/stuff', (req, res, next) => {

    // Delete the _ID parameter from the request why is'nt not correct for this request
    delete req.body._id;

    const thing = new Thing({ ...req.body });

    thing.save()
        .then( () => res.status(201).json({ message: 'Objet enregistré !'}) )
        .catch( error => res.status(400).json({ error }) );
});



/**
 * PUT request: for Update a Thing object
 */
app.put('/api/stuff/:id', (req, res, next) => {

    Thing.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Objet modifié !'}))
        .catch( error => res.status(400).json({ error }));
});



app.delete( '/api/stuff/:id', (req, res, next) => {

   Thing.deleteOne({_id: req.params.id })
       .then( () => res.status(200).json({ message: 'Objet supprimé avec succès' }))
       .catch( error => res.status(400).json({ error }));
});



/**
 * GET request:  for get object wih a dynamic parameter in URL
 */
app.get('/api/stuff/:id', (req, res, next) => {

    Thing.findOne({ _id: req.params.id })
        .then(  thing => res.status(200).json(thing))
        .catch( error => res.status(404).json({ error }));
});



/**
 * GET Request: Get all Articles
 */
app.get('/api/stuff', (req, res, next) => {

    Thing.find()
        .then(  things => res.status(200).json(things) )
        .catch( error => res.status(400).json({ error }) );

});





// Export Express module for use in application
module.exports = app;