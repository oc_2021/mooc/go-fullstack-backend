
// Import packages
const mongoose = ( require( 'mongoose' ) );


// Create the Mongoose Database Model Schema
const thingSchema = mongoose.Schema({
    title:          { type: String, required: true },
    description:    { type: String, required: true },
    imageUrl:       { type: String, required: true },
    price:          { type: Number, required: true },
    userId:         { type: String, required: true },

});


// Export the mongoose model schema
module.exports = mongoose.model( 'Thing', thingSchema );

